#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char* string;

typedef	struct {
	int num_maison;
	char quartier[20];
	char ville[20];
}Adresse;

typedef struct {
	int jour;
	int mois;
	int ans;
}Date;

typedef struct
{
	int num_compte;
	char nom[30];
	char prenom[30];
	Date date;	// date de naissance.
	char cin[10];		// numero de la carte nationale.
	int telephone;	// numero de compte.
	Adresse adresse;
	float balance;
}Client;

int compter_lignes(string filename)
{
	/*
		arguments : le nom/chemin du fichier contenant les donnes des clients.
		fonctionnement : lire charactere par character , si il est \n ajouter 1
			variable nombre_de_ligne(\n designe une nouvelle ligne)
		valeur de retour : le nombre de ligne , type int
		exemple : compter_lignes("/home/ablil98/mini_projet/test.csv")
	*/

	FILE* file = fopen(filename, "r");

	int ch, nombre_de_lignes = 0;
	do
	{
	    ch = fgetc(file);
	    if(ch == '\n')
	        nombre_de_lignes++;
	}
	while (ch != EOF);
	/*	if(ch != '\n' && nombre_de_lignes != 0)
		    nombre_de_lignes++;
	*/
	fclose(file);
	return nombre_de_lignes;
}

void read_data(FILE *file, Client *data, int linenumber)
{
	// read data form the file and store them in the data array
	// le fichier doit etre ouvert en mode lecture
	int line;
	for (line = 0; line <= linenumber; ++line)
	{
		fscanf(file, "%d %s %s %d %d %d %s %d %d %s %s %f", &data->num_compte, data->nom, data->prenom, &data->date.jour, &data->date.mois, &data->date.ans, data->cin, &data->telephone, &data->adresse.num_maison, data->adresse.quartier, data->adresse.ville, &data->balance);
        data++;
	}
}



int menu()
{
    int i ;
    do {
        puts("-------------------------------");
        printf("1> Creer un nouveau compte.\n");
        printf("2> Supprimer un compte. \n");
        printf("3> Afficher tous les comptes.\n");
        printf("4> Modifier des informations d\'un compte.\n");
        printf("5> Trier les balances. \n");
        printf("6> Diposer/Verser d\'argent. \n");
        printf("7> Rechercher un compte. \n");
        printf("0> Quitter le programme.\n");
        puts("-------------------------------");
        printf("> ");
        scanf("%d",&i);
    } while (i > 7 || i < 0);
    return i;
}

Client obtenir_infos()
{
	// prendre le donner d'utilisateur et les enregister dans une strucure retourne a la fin
	// fonction non defint dans traiterlesfichier.h

	Client tmp; // variable temporaire

    printf("entrer votre nom : ");
    scanf("%s",tmp.nom);

    printf("entrer votre prenom : ");
    scanf("%s",tmp.prenom);

    printf("entrer la date de naissance : \n jour : ");
    scanf("%d",&tmp.date.jour);
    printf(" mois : ");
    scanf("%d",&tmp.date.mois);
    printf(" annee : ");
    scanf("%d",&tmp.date.ans);
    printf("entrer votre cin : ");
    scanf("%s",tmp.cin);
    printf("entrer votre numero de telephone : ");
    scanf("%d",&tmp.telephone);
    printf("entrer le num de maison : ");
    scanf("%d", &tmp.adresse.num_maison);
    printf("entrer le quartier : ");
    scanf("%s",tmp.adresse.quartier);
    printf("entrer votre ville : ");
    scanf("%s",tmp.adresse.ville);
    printf("Enter vore balance initiale : ");
    scanf("%f", &tmp.balance);
    return tmp;
}

void store_data(string filename, Client client, int nomber_de_client)
{
	// enregister le donnees dans le fichier
	// l'argument Client client est la fonction obtenir info

	FILE *file = fopen(filename, "a");
	if (!file)
	{
		puts("Un erreur s\'est produit ! \n fermeture du programme\n");
		exit(0);
	}
	client.num_compte = nomber_de_client++;   // incrementer le numero de compte existant
	fprintf(file, "%d %s %s %d %d %d %s %d %d %s %s %f\n", client.num_compte, client.nom, client.prenom, client.date.jour, client.date.mois, client.date.ans, client.cin, client.telephone, client.adresse.num_maison, client.adresse.quartier, client.adresse.ville, client.balance);
	fclose(file);
	exit(1); // exit the programm after adding acount
}

void ouvrir_compte()
{
	Client tmp = obtenir_infos(); // structure temporaire
	int nombre_de_clients = compter_lignes("data.txt"); // avoir le nombre de client existant pour incrementer le numero de compte de ce nouveau client
	store_data("data.txt", tmp, nombre_de_clients); // stocker les infos du nouveau client dans le ficher et sortir du programme
}

int cherche_compte(Client *data, int nombre_de_clients, string nom, string prenom)
{
    int i;
    for (i = 0; i < nombre_de_clients; ++i)
    {
        // comparer les noms
        if (!(strcmp((data+i)->nom, nom)))
        {
            // comparer les prenoms
            if (!(strcmp((data+i)->prenom, prenom)))
            {
                // retourner l'indice ou le num de compte du client
                return i;
            }
        }
    }
    return -1;  // retourner -1 si le client n'existe pas
}

void supprimer_compte(Client *data, int indice_client,int nombre_de_clients, Client *newdata)
{
	// supprimer un client du tableau et retourner un nouveau tableau ,avec lequel on va travailler
    Client *client;
	for (client=data; client < data+nombre_de_clients; client++)
	{
		if (client->num_compte != indice_client)
		{
			*(newdata ) = *(client);
			newdata++;
		}
	}
}

void afficher_tous_compte(Client *DATA, int taille)
{
	// afficher tour les compte du fichier
	Client *p;
	int i = 1;
	for (p = DATA; p < DATA + taille; p++)
	{
		printf("CLIENT %d\n", i++);
		printf("NOM : %s PRENOM : %s BALANCE : %f DH ", p->nom, p->prenom, p->balance);
		printf("CIN : %s DATE DE NAISSANCE : %d %d %d\n", p->cin, p->date.jour, p->date.mois, p->date.ans);
		printf("TELEPHONE : %d ADRESSE LOCALE : %d, %s %s\n", p->telephone, p->adresse.num_maison, p->adresse.quartier, p->adresse.ville);
		puts("-----------------------------------");
	}
}

void modifier_compte(Client *client)
{
    printf("entrer le nouveau nom : ");
    scanf("%s",client->nom);
    printf("entrer le nouveau prenom : ");
    scanf("%s",client->prenom);
    printf("entrer la date de naissance : \n jour : ");
    scanf("%d",&client->date.jour);
    printf("mois : ");
    scanf("%d",&client->date.mois);
    printf("annee : ");
    scanf("%d",&client->date.ans);
    printf("entrer le nouveau cin : ");
    scanf("%s",client->cin);
    printf("entrer le numero de telephone : ");
    scanf("%d",&client->telephone);
    printf("entrer le num de maison : ");
    scanf("%d", &client->adresse.num_maison);
    printf("entrer le quartier : ");
    scanf("%s",client->adresse.quartier);
    printf("entrer la ville : ");
    scanf("%s",client->adresse.ville);
}

void double_tableau(Client *original, int taille, Client *nouveau)
{
	// data est le tableau original
	// ddata est le double tableau
	int i = 0;
	while ( i < taille){
		*(nouveau+i) = *(original+i);
		i++;
	}
}

void tri_balance(Client *data, int nombre_de_clients)
{
	// avant d'utilise cette fonction il faut utiliser les fonctions : double_tableau et afficher_tri_balance
	int final_counter; // une variable pour sortir de la boucle
	do {
		int counter ; // le compteur du permutation
		int i;
		for (i = 0 ; i < nombre_de_clients-1; i++) {
			if ( (data + i)->balance > (data + i + 1)->balance) {
				// permuter les deux clients
				Client tmp ;
				tmp = *(data+i+1);
				*(data+i+1) = *(data+i);
				*(data+i) = tmp;
				counter +=1;
			}
		}
		final_counter = counter;
		counter = 0; // remedire le compteur a zero et continuer encore la boucle for

	} while ( final_counter != 0 ); // sortir de la boucle si le nombre de permutation est null
}

void afficher_tri_balance(Client *data, int nombre_de_clients)
{
	// cettte fonction afficher seulement le nom et le prenom et le balance
	// afficher tour les compte du fichier
	Client *p;
	int i = 1;
	for (p = data; p < data + nombre_de_clients; p++){
		printf("NOM : %s PRENOM : %s BALANCE : %f DH\n", p->nom, p->prenom, p->balance);
	}
}

void manipuler_balance(Client *client)
{
    int i;
    float N; //le montant.
    do
    {
        puts("-----------------");
        printf("1-> verser de l\'argent . \n");
        printf("2-> diposer de l\'argent . \n");
        printf("3-> quitter la manipulation.\n");
        puts("--------------------");
        scanf("%d",&i);
    }
    while(i<1 || i>3);
    if (i==1)
    {
        printf("donner la somme dargent a verser : ");
        scanf("%f",&N);
        client->balance -= N;
        if (client->balance < 0)
        {
        	client->balance =0;
        }
        printf("le nouveau solde est : %f \n",client->balance);
    }
    if (i==2)
    {
        printf("donner la somme dargent a diposer : ");
        scanf("%f",&N);
        client->balance += N;
        printf("le nouveau solde est : %f \n",client->balance);
    }
}

void afficher_un_compte(Client *client)
{
    printf("le num du compte : %d \n", client->num_compte);
    printf("le nom : %s \n", client->nom);
    printf("le prenom : %s \n", client->prenom);
    printf("la date de naissance : %d \\ %d \\ %d \n", client->date.jour,client->date.mois,client->date.ans);
    printf("le CIN : %s \n", client->cin);
    printf("le telephone : %d \n", client->telephone);
    printf("ladresse : %d - %s - %s \n", client->adresse.num_maison,client->adresse.quartier,client->adresse.ville);
    printf("le solde : %f \n", client->balance);
}

void mise_a_jour(string filename, Client *data, int nombre_de_clients)
{
	// enrigister les nouveau donne dans le fichier
	// cette fonction doit etre appelle a la fin du programme
	FILE *file = fopen(filename, "w");
	if (!file)
	{
		puts("Un erreur s\'est produit ! \n fermeture du programme\n");
	}
	int i;
	for (i = 0 ; i < nombre_de_clients; i++){
		int num_compte = i;
		fprintf(file, "%d %s %s %d %d %d %s %d %d %s %s %f\n", num_compte, (data+i)->nom, (data+i)->prenom, (data+i)->date.jour, (data+i)->date.mois, (data+i)->date.ans, (data+i)->cin, (data+i)->telephone, (data+i)->adresse.num_maison, (data+i)->adresse.quartier, (data+i)->adresse.ville, (data+i)->balance);
	}
	fclose(file);
	exit(0);
}

int main()
{

	// instruction initiale
	int nombre_de_clients = compter_lignes("data.txt"); // obtenir le nombre de clients existant
	Client *data = (Client*)malloc(sizeof(Client)*nombre_de_clients);	// creer un tableau de structure pour stocker les donnees du clients


	// ouvrir le fichier et sotcker les donnes dans un tableau
	FILE *file = fopen("data.txt", "r");
	if (!file)
	{
		puts("Erreur d\'ouverture du fichier !");
		exit(3);
	}
	read_data(file, data, nombre_de_clients); // stocker les donnes dans le tableau
	fclose(file);
	while (1)
	{
		// une fois une actions et fait le menu va apparaitre une autre fois avec la boucle while

		// aficher le menu
		int choix = menu();

		// traiter les choix
		switch (choix)
	    {
	    case 1 :	// creer
	    	// le programme s'est termine apres cette fonction
	        ouvrir_compte();
	        break;

	    case 2 :	// supprimer
	    	{
	    		// variable pour stocker le nom et le prenom du client a supprimer
	    		string nom = (string)malloc(sizeof(char)*30);
	    		string prenom = (string)malloc(sizeof(char)*30);

	    		// prendre le nom et le prenom du client a supprimer
	    		printf("Enter le nom : ");scanf("%s", nom);
	    		printf("Enter le prenom : ");scanf("%s", prenom);

	    		// chercher le client, et retourner son indice dans le tableau
	    		int indice_client = cherche_compte(data, nombre_de_clients, nom, prenom);

	    		// stocker les donnes du client dans une structure et le supprimer apres
	    		if (indice_client != -1)
	    		{
	    			Client *newdata = (Client*)malloc(sizeof(Client)*(nombre_de_clients-1));
	    			printf("Client Trouvee \n");
	    			// supprimer le client
	    			supprimer_compte(data, indice_client, nombre_de_clients, newdata);
	    			nombre_de_clients -=1;
	    			data = newdata;
	    			printf("Client supprmiee avec succes\n");
	    		}
	    		else
	    		{
	    			printf("Client Introuvable\n");
	    		}
	        }
	        break;

	    case 3 : 	// afficher tous
	        afficher_tous_compte(data, nombre_de_clients);
	        break;

	    case 4 :	// modifier
	    	{
	    		// variable pour stocker le nom et le prenom
	    		string nom = (string)malloc(sizeof(char)*30);
	    		string prenom = (string)malloc(sizeof(char)*30);

	    		// prendre le nom et le prenom du client a modifier
	    		printf("Enter le nom : ");scanf("%s", nom);
	    		printf("Enter le prenom : ");scanf("%s", prenom);

	    		// chercher le client, et retourner son indice dans le tableau
	    		int indice_client = cherche_compte(data, nombre_de_clients, nom, prenom);

	    		// modifier le compte du client s'il existe
	    		if (indice_client != -1)
	    		{
	    			printf("Client Trouvee \n");
	    			Client *client_a_modifier = (Client*)malloc(sizeof(Client));
	    			client_a_modifier = data + indice_client;
	    			// modifier les infos d'un client
	    			modifier_compte(client_a_modifier);
	    			puts("Informations modifie avec succes.");
	    		}
	    		else{
	    			printf("Client introuvable\n");
	    		}
	    	}
	        break;

	    case 5 :	// tri
	    	{
		    	Client *nouveau = (Client*)malloc(sizeof(Client)*nombre_de_clients); // creer un nouveau tableau a modifier
		    	double_tableau(data, nombre_de_clients, nouveau); // copier le tableau original dans le nouveau tableau
		        tri_balance(nouveau, nombre_de_clients); // trier le nouveau tableau sans toucher l'orginal
		        afficher_tri_balance(nouveau, nombre_de_clients); // afficher le tableau apres le tri
	   		}
	        break;

	    case 6 :
	    	{
	    		// variable pour stocker le nom et le prenom a rechercher
	    		string nom = (string)malloc(sizeof(char)*30);
	    		string prenom = (string)malloc(sizeof(char)*30);

	    		// prendre le nom et le prenom du client a afficher
	    		printf("Enter le nom : ");scanf("%s", nom);
	    		printf("Enter le prenom : ");scanf("%s", prenom);

	    		// chercher le client, et retourner son indice dans le tableau
	    		int indice_client = cherche_compte(data, nombre_de_clients, nom, prenom);

	    		// afficher le compte du client s'il existe
	    		if (indice_client != -1)
	    		{
	    			manipuler_balance(data+indice_client);
	    		}
	    		else{
	    			printf("Client introuvable\n");
	    		}
	    	}
	    	break;
	    case 7: // afficher un compte
	    	{
	    		// variable pour stocker le nom et le prenom a rechercher
	    		string nom = (string)malloc(sizeof(char)*30);
	    		string prenom = (string)malloc(sizeof(char)*30);

	    		// prendre le nom et le prenom du client a afficher
	    		printf("Enter le nom : ");scanf("%s", nom);
	    		printf("Enter le prenom : ");scanf("%s", prenom);

	    		// chercher le client, et retourner son indice dans le tableau
	    		int indice_client = cherche_compte(data, nombre_de_clients, nom, prenom);

	    		// afficher le compte du client s'il existe
	    		if (indice_client != -1)
	    		{
	    			afficher_un_compte(data+indice_client);
	    		}
	    		else{
	    			printf("Client introuvable\n");
	    		}
	    	}
	    	break;
	    case 0 :
	    	{
	    		mise_a_jour("data.txt", data, nombre_de_clients);
	    		exit(2);
	        	break;
	    	}
	    }
	}
	fclose(file);
	return 0;
}

